var gulp = require('gulp');
var concat = require('gulp-concat');
var connect = require('gulp-connect');
var browserify = require('gulp-browserify');
var rename = require('gulp-rename');
var insert = require('gulp-insert');
var watch = require('gulp-watch');
var wiredep = require('wiredep').stream;

var genNote = {
    js: '// NOTE: This file has been generated automatically\n',
    html: '<!-- NOTE: This file has been generated automatically -->\n'
};
var paths = {
    src:{
        coffee: 'src/coffee/main.coffee',
        html: 'src/index.tpl.html'
    },
    build: {
        jsName: 'game.js',
        jsPath: './build/js',
        html: 'index.html'
    }
};

gulp.task('default', ['coffee',  'bower']);

gulp.task('run', ['watch', 'webserver']);

gulp.task('watch', ['coffee:watch','bower:watch']);

gulp.task('bower', function() {
  gulp.src(paths.src.html)
   .pipe(concat(paths.build.html))
   .pipe(gulp.dest('./'))
   .pipe(wiredep())
   .pipe(insert.prepend(genNote.html))
   .pipe(gulp.dest('./'));
});

gulp.task('webserver', function() {
  connect.server({
    port: 8080,
    livereload: true,
  });
});

gulp.task('coffee', function() {
  gulp.src(paths.src.coffee, { read: false })
    .pipe(browserify({
      transform: ['coffeeify'],
      extensions: ['.coffee']
    }))
    .pipe(rename(paths.build.jsName))
    .pipe(insert.prepend(genNote.js))
    .pipe(gulp.dest(paths.build.jsPath));
});

// watches for html changes
gulp.task('bower:watch', function() {
    gulp.watch(paths.src.html, ['bower']);
});

// watches for coffeescript changes
gulp.task('coffee:watch', function() {
    gulp.watch(paths.src.coffee, ['coffee']);
});
